package wpardel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class POP3 
{	
	List<Email> LastProgramRunEmails = new ArrayList<Email>();;
	List<Email> Emails = new ArrayList<Email>();
	public Socket Connection;
	private String server ;
	private String UserName;
	private String Passwd;
	private  int PORT ;
	public int TIME;
	BufferedReader breader;
	BufferedWriter bwriter;
	Reader reader ;
	public POP3()
	{
		this.LoadSettingsFromXML();
		Connect();
		System.out.println(SingleResponse());
		Auth(this.UserName,this.Passwd);
		List<Email> newEmails = GatherAllEmailsWithUIDL();
		CheckForNewMessages(newEmails,true);
		this.Emails = newEmails;
		try 
		{
			Disconnect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void LoadSettingsFromXML()
	{
			
		try
		{
			File file = new File("Options.xml");
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
			        .newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(file);
			this.server = document.getElementsByTagName("Server").item(0).getTextContent();
			this.PORT = Integer.valueOf(document.getElementsByTagName("Port").item(0).getTextContent());
			this.UserName = document.getElementsByTagName("Username").item(0).getTextContent(); 
			this.Passwd = document.getElementsByTagName("Pass").item(0).getTextContent();
			this.TIME = Integer.valueOf(document.getElementsByTagName("CheckTime").item(0).getTextContent()) * 1000;

			int t =5;
		} 
		catch (SAXException e)
		{			
			e.printStackTrace();
		} 
		catch (IOException e) 
		{			
			e.printStackTrace();
		}
		catch (ParserConfigurationException e)
		{
			e.printStackTrace();
		}
	}
	public void CheckForEmails()
	{		
		Connect();
		Auth(this.UserName,this.Passwd);		
		List<Email> newEmails = GatherAllEmailsWithUIDL();
		CheckForNewMessages(newEmails,false);
		try 
		{
			Disconnect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public boolean Connect() 
	{
		Connection = new Socket();		
		try
		{
			Connection.connect(new InetSocketAddress(server, PORT));
			
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			return false;
		}		
		try 
		{
			breader = new BufferedReader(new InputStreamReader(Connection.getInputStream()));
			reader = new InputStreamReader(Connection.getInputStream(), "UTF-8");
			bwriter = new BufferedWriter(new OutputStreamWriter(Connection.getOutputStream()));	
			//System.out.println("CONNECTED RESPONSE = " + ReadResponseFromServer());
		}
		catch (IOException e)
		{
			return false;
		}
		return true;
	}
	public boolean Disconnect() throws IOException
	{
		breader = null;
		bwriter = null;
		reader = null;
		if(!Connection.isConnected())return true;
		Connection.close();
		return true;
	}	
	public String ReadAllResponseFromServer()
	{		
		if(Connection.isConnected())
		{
			String response="";
			try 
			{
				String line="";
				//Thread.sleep(100);
				while( true)//(line = breader.readLine()) != null )
				{				
					line = breader.readLine();
		            if (line.equals(".") ){//|| line.equals("") || line.equals(" ")) {
		                // No more lines in the server response
		            	//System.out.println("LINE IS .");
		                break;
		            }
		            if ((line.length() > 0) && (line.charAt(0) == '.')) {
		            	//System.out.println("LINE IS SOMETHING");
		                // The line starts with a "." - strip it off.
		                line = line.substring(1);
		            }
		            //if(line.isEmpty()){System.out.println("LINE IS EMPTY"); break;}
		            response += line+"\n" ;
		            //System.out.println("WHILE IS LOOPING");
		            line = null;
		            
				}
				//System.out.println("OUT OF LOOP");
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				System.out.println("IO EXCEPTION");
				e.printStackTrace();
			}
			//System.out.println("SERVER RESPONDED WITH = " + response);
			return response;			
		}
		else
		{
			return null;
		}
	}
	public String SingleResponse()
	{
		if(Connection.isConnected())
		{
			try
			{
				return breader.readLine();				
			}
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	public void WriteToServer(String str)
	{
		if(Connection.isConnected())
		{
			try 
			{
				bwriter.write(str + "\n");
				bwriter.flush();
				//ReadResponseFromServer();
			}
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public boolean Auth(String Login,String Pass)
	{
		System.out.println("Preforming Auth");
		WriteToServer("USER "+Login);		
		System.out.println("NOW PASS= " + SingleResponse());
		WriteToServer("PASS " + Pass);
		System.out.println("SERVER AUTH RESPONSE = "+SingleResponse());
		return true;
	}
	public void Exit()
	{
		System.out.println("Preform Exit");
		WriteToServer("QUIT");
		//System.out.println("SERVER RESPONSE = " + ReadResponseFromServer());
		try 
		{
			Disconnect();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			System.out.println("Already disconnected");
		}
	}	
	
	public List<Email> GatherAllEmailsWithUIDL()
	{
		WriteToServer("UIDL");		
		String [] Response = ReadAllResponseFromServer().split("\n");
		//System.out.println("UIDL = \n" +Response.toString());
		List<Email> newEmails = new ArrayList<Email>();
		Pattern pat = Pattern.compile("(Subject:)(\\s|Re:)(.*)");
		for(String tmp : Response)
		{
			if(tmp.equals("+OK")){continue;}
			String [] splitet = tmp.split(" ");
			WriteToServer("RETR "+splitet[0]);
			SingleResponse();
			String res = ReadAllResponseFromServer();
			Matcher m = pat.matcher(res);		
			m.find();
			//System.out.println("TOPIC = " + m.group(3));
			
			/*System.out.println("RETR = " + SingleResponse());
			System.out.println("RETR = " + SingleResponse());

			System.out.println("RETR = " + SingleResponse());*/


			Email email = new Email();
			email.UIDL = splitet[1];
			email.Title = m.group(3);
			newEmails.add(email);
		}
		return newEmails;
		
	}
	public void CheckForNewMessages(List<Email> newEmails,boolean initProgram)
	{
		List<Email> News = new ArrayList<Email>();
		// Check for new 
		int newmessages = 0;
		for(Email New : newEmails)
		{
			boolean match2 = false;
			for(Email Old : Emails )
			{
				if(New.UIDL.equals(Old.UIDL))
				{
					match2 = true;
				}
			}
			if(!match2){News.add(New);newmessages++;};
			
		}
		// Print Status
		Emails = newEmails;
		if(!initProgram && newmessages > 0){
			System.out.println("Preforming CHECK .....");
			System.out.println("NEW MESSAGES = "+newmessages);
			for(Email e : News)
			{
				System.out.println("Message UIDL = "+e.UIDL+" TOPIC = "+e.Title);
			}
		}
		if(!initProgram && newmessages == 0){
			System.out.println("Preforming CHECK .....");
			System.out.println("NO NEW MESSAGES");
		}
	}
}
